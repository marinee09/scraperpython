<?php
try
{
    $BDD = new PDO('mysql:host=127.0.0.1;dbname=citations;charset=utf8', 'root', '');
}

catch(Exception $e)
{
    echo 'Echec de la connexion à la base de données : '.$e->getMessage();
    exit();
}

$url = "test.json";
$json = file_get_contents($url);
$result = json_decode($json,true);
foreach($result as $key => $value)
{   
    //var_dump($value['text']);
    
    if($value)
    {   
       $req = $BDD->prepare('INSERT INTO citation(text,auteur,thematique)
        VALUES (?, ?, ?)');

        $text = $value['text'];
        $auteur = $value['auteur'];
        $thematique = $value['thematique'];

        $req->bindParam(1, $text);
        $req->bindParam(2, $auteur);
        $req->bindParam(3, $thematique);

        $req->execute();
    }
}
?>


