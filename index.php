<?php
try
{
    $BDD = new PDO('mysql:host=127.0.0.1;dbname=citations;charset=utf8', 'root', '');
} catch (Exception $e) {
    echo 'Echec de la connexion à la base de données : ' . $e->getMessage();
    exit();
}
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css"/>
    <title>Document</title>
</head>
<body>

<?php
$citations = $BDD->prepare('SELECT * FROM citation');
$citations->execute();
$reqcitations = $citations->fetchall();
?>
<div class="container">
    <div class="row">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col-5">Citations </th>
                        <th scope="col-4">Auteur</th>
                        <th scope="col-3">Thematique</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($reqcitations as $reqcitation) {?>
                        <tr>
                            <td scope="col-6"id="center" ><?php echo $reqcitation["text"] ?> </td>
                            <td scope="col-3"id="center"><?php echo $reqcitation["auteur"] ?> </td>
                            <td scope="col-3" id="center"><?php echo $reqcitation["thematique"] ?> </td>
                        </tr>
                    <?php }?>
                </tbody>
            </table>
            </div>
        </div>
</body>
</html>

