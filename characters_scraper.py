# -*- coding: utf-8 -*-
import scrapy


class QuotesSpider(scrapy.Spider):
    i = 0
    name = "quotes"
    start_urls = [
        'http://citation-celebre.leparisien.fr',
    ]

    def parse(self, response):
        #div.class
        for quote in response.css('div.citation'):
            yield {
                'text': quote.css('div.laCitation p.laCitation q a::text').extract_first(),
                'auteur': quote.css('div.auteur div.additionalInformation a::text').extract_first(),
                'thematique': quote.css('.citation h3 a::text').extract_first(),

            }

        print( str(self.i) + " page scrappees.")

        next_page = response.urljoin("http://citation-celebre.leparisien.fr/liste-citation?page="+str(self.i))
        self.i = self.i + 1
        if self.i > 30:
            print ("\n30 pages scrappees\n.")
            exit()
        yield scrapy.Request(next_page, callback=self.parse) 